package com.basar.administrator.activity;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.administrator.R;
import com.basar.administrator.helper.Config;
import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;
import com.google.zxing.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import libs.basar.prettydialog.PrettyDialog;
import libs.basar.prettydialog.PrettyDialogCallback;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends BasarActivity  implements ZXingScannerView.ResultHandler, OnPermissionCallback, View.OnClickListener
{

    private static final String TAG = "scan_aktifitas";
    private ZXingScannerView m_scanner_view;

    private ProgressBar loading;


    private  String m_error = "";

    private  String m_success = "";

    private PermissionHelper permission_helper;


    private boolean is_single;
    private AlertDialog alert_dialog;
    private String[] needed_permission;
    private final static String[] MULTI_PERMISSIONS = new String[]
    {
            Manifest.permission.VIBRATE,
            Manifest.permission.INTERNET,
            Manifest.permission.CAMERA

    };

    private  PrettyDialog m_pretty_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        initComponent();
        initVariable();
        initEvent();
        mainLogic();
    }


    public  void deviceConfirm(Result _raw_result)
    {
        final Result scan_result = _raw_result;

        m_scanner_view.stopCamera();
        setContentView(R.layout.activity_scan);
        loading  = findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);
        loading.bringToFront();
        if(scan_result.getBarcodeFormat().toString().equalsIgnoreCase("QR_CODE"))
        {
            String alamat_url = Config.getAppProtocol()+ Config.getDomain(getBaseContext())+"/index.php";
            Log.v(TAG, "alamat" + alamat_url);
            StringRequest string_request = new StringRequest(Request.Method.POST, alamat_url,
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response)
                        {
                            loading.setVisibility(View.GONE);
                            Log.v(TAG, "responnya :" + response);
                            try
                            {
                                JSONObject json_object = new JSONObject(response);
                                String result = json_object.getString("status");
                                Log.v(TAG, "dan hasilnya: " + result);
                                if (result.equalsIgnoreCase("success"))
                                {
                                    final PrettyDialog pretty_dialog = new PrettyDialog(ScanActivity.this);
                                    pretty_dialog.setIcon(R.drawable.pdlg_icon_info)
                                            .setSound(R.raw.warning)
                                            .setMessage(json_object.getString("desc").toString())
                                            .addButton(
                                                    getResources().getString(R.string.dialog_button_approve),					// button text
                                                    R.color.label_text,		// button text color
                                                    R.color.button_success,		// button background color
                                                    new PrettyDialogCallback()
                                                    {
                                                        // button OnClick listener
                                                        @Override
                                                        public void onClick()
                                                        {
                                                            deviceActivation(scan_result.getText() );
                                                        }
                                                    }
                                            )
                                            .addButton(
                                                getResources().getString(R.string.dialog_button_cancel),					// button text
                                                R.color.label_text,		// button text color
                                                R.color.button_danger,		// button background color
                                                new PrettyDialogCallback()
                                                {
                                                    // button OnClick listener
                                                    @Override
                                                    public void onClick()
                                                    {
                                                        startActivity(new Intent(getBaseContext(), MainActivity.class));
                                                    }
                                                }
                                            ).setAnimationEnabled(true).showDialog();


                                }
                                else
                                {
                                    m_error = json_object.getString("desc").toString();
                                    m_pretty_dialog.setIcon(R.drawable.pdlg_icon_close).setIconTint(R.color.button_danger)
                                            .setIconCallback(new PrettyDialogCallback()
                                            {
                                                @Override
                                                public void onClick()
                                                {
                                                    m_pretty_dialog.dismiss();
                                                    startActivity(new Intent(getBaseContext(), MainActivity.class));
                                                }
                                            }).setMessage(m_error).setSound(R.raw.error).showDialog();


                                }

                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            m_error = error.toString();

                            m_pretty_dialog.setIcon(R.drawable.pdlg_icon_close).setIconTint(R.color.button_danger)
                                    .setIconCallback(new PrettyDialogCallback()
                                    {
                                        @Override
                                        public void onClick()
                                        {
                                            m_pretty_dialog.dismiss();
                                            startActivity(new Intent(getBaseContext(), MainActivity.class));
                                        }
                                    }).setMessage(m_error).setSound(R.raw.error).showDialog();
                        }
                    }
            )
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<>();

                    params.put("TOKEN_KEY", Config.getLoginToken(getBaseContext()));
                    params.put("page","admin");
                    params.put("type","model");
                    params.put("action","token-info");
                    params.put("device-token",scan_result.getText());

                    return params;
                }
            };

            RequestQueue request_queue = Volley.newRequestQueue(this);
            //maksimal 27 detik
            string_request.setRetryPolicy(new DefaultRetryPolicy(
                    Config.getVolleyMaxResponse()
                    ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                    ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            request_queue.add(string_request);
        }
        else
        {
            m_error = "barcode harus berformat QRCODE";
        }
    }

    public  void deviceActivation(final String _token_device)
    {
        loading  = findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);
        loading.bringToFront();
        String alamat_url = Config.getAppProtocol()+ Config.getDomain(getBaseContext())+"/index.php";
        Log.v(TAG, "alamat" + alamat_url);
        StringRequest string_request = new StringRequest(Request.Method.POST, alamat_url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        loading.setVisibility(View.GONE);
                        Log.v(TAG, "responnya :" + response);
                        try
                        {
                            JSONObject json_object = new JSONObject(response);
                            String result = json_object.getString("status");
                            Log.v(TAG, "dan hasilnya: " + result);
                            if (result.equalsIgnoreCase("success"))
                            {
                                m_pretty_dialog.setIcon(R.drawable.pdlg_icon_success).setIconTint(R.color.button_success)
                                        .setIconCallback(new PrettyDialogCallback()
                                        {
                                            @Override
                                            public void onClick()
                                            {
                                                m_pretty_dialog.dismiss();
                                                startActivity(new Intent(getBaseContext(), MainActivity.class));

                                            }
                                        }).setMessage(json_object.getString("desc").toString()).setSound(R.raw.success).showDialog();
                            }
                            else
                            {
                                m_pretty_dialog.setIcon(R.drawable.pdlg_icon_close).setIconTint(R.color.button_danger)
                                        .setIconCallback(new PrettyDialogCallback()
                                        {
                                            @Override
                                            public void onClick()
                                            {
                                                m_pretty_dialog.dismiss();
                                                startActivity(new Intent(getBaseContext(), MainActivity.class));

                                            }
                                        }).setMessage(json_object.getString("desc").toString()).setSound(R.raw.error).showDialog();
                            }

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        m_error = error.toString();
                        m_pretty_dialog.setIcon(R.drawable.pdlg_icon_close).setIconTint(R.color.button_danger)
                                .setIconCallback(new PrettyDialogCallback()
                                {
                                    @Override
                                    public void onClick()
                                    {
                                        m_pretty_dialog.dismiss();
                                        startActivity(new Intent(getBaseContext(), MainActivity.class));

                                    }
                                }).setMessage(m_error).setSound(R.raw.error).showDialog();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();

                params.put("TOKEN_KEY", Config.getLoginToken(getBaseContext()));
                params.put("page","admin");
                params.put("type","model");
                params.put("action","device-aktifasi");
                params.put("device-token",_token_device.toString());

                return params;
            }
        };

        RequestQueue request_queue = Volley.newRequestQueue(this);
        //maksimal 27 detik
        string_request.setRetryPolicy(new DefaultRetryPolicy(
                Config.getVolleyMaxResponse()
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request_queue.add(string_request);
    }
    @Override
    public void onResume()
    {
        super.onResume();
        m_scanner_view.setResultHandler(this); // Register ourselves as a handler for scan results.
        m_scanner_view.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause()
    {
        super.onPause();
        m_scanner_view.stopCamera();           // Stop camera on pause
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        m_scanner_view.stopCamera();// Stop camera on pause
    }


    @Override
    public void handleResult(Result raw_result)
    {
        Log.v(TAG, raw_result.getText()); // Prints scan results
        Log.v(TAG, raw_result.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        //MainActivity.tvresult.setText(rawResult.getText());

        Vibrator getar = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);// Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= 26)
        {
            getar.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
        }
        else
        {
            getar.vibrate(200);
        }
        deviceConfirm(raw_result);


    }
    @Override
    protected  void initComponent()
    {

        m_scanner_view = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(m_scanner_view);
    }
    @Override
    protected  void initVariable()
    {

        m_pretty_dialog = new PrettyDialog(this);
        permission_helper = PermissionHelper.getInstance(this);
        permission_helper
                .setForceAccepting(false) // default is false. its here so you know that it exists.
                .request( MULTI_PERMISSIONS);
    }

    @Override
    protected  void initEvent()
    {

    }

    @Override
    protected  void mainLogic()
    {
    }


    /*---------------begin permission helper--------------------*/
    /**
     * Used to determine if the user accepted {@link android.Manifest.permission#SYSTEM_ALERT_WINDOW} or no.
     * <p/>
     * if you never passed the permission this method won't be called.
     */
    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        permission_helper.onActivityForResult(requestCode);
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permission_helper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override public void onPermissionGranted(@NonNull String[] permissionName) {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Granted");
        //downloadDashboard();
        Log.v("PERMISSION onPermissionGranted", "Permission(s) " + Arrays.toString(permissionName) + " Granted");
    }

    @Override public void onPermissionDeclined(@NonNull String[] permissionName) {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Declined");
        Bundle bundle = new Bundle();
        bundle.putString("data_error", "memerlukan perizinan");
        Intent intent = new Intent(ScanActivity.this, ErrorActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.i("PERMISSION onPermissionDeclined", "Permission(s) " + Arrays.toString(permissionName) + " Declined");
    }

    @Override public void onPermissionPreGranted(@NonNull String permissionsName) {
        //result.setText("Permission( " + permissionsName + " ) preGranted");
        Log.i("PERMISSION onPermissionPreGranted", "Permission( " + permissionsName + " ) preGranted");
    }

    @Override public void onPermissionNeedExplanation(@NonNull String permissionName) {
        Log.i("PERMISSION NeedExplanation", "Permission( " + permissionName + " ) needs Explanation");
        if (!is_single)
        {
            needed_permission = PermissionHelper.declinedPermissions(this, MULTI_PERMISSIONS);
            StringBuilder alert_dialog = new StringBuilder(needed_permission.length);
            if (needed_permission.length > 0)
            {
                for (String permission : needed_permission)
                {
                    alert_dialog.append(permission).append("\n");
                }
            }
            //result.setText("Permission( " + alert_dialog.toString() + " ) needs Explanation");
            AlertDialog alert = getAlertDialog(needed_permission, alert_dialog.toString());
            if (!alert.isShowing())
            {
                alert.show();
            }
        }
        else
        {
            //result.setText("Permission( " + permissionName + " ) needs Explanation");
            getAlertDialog(permissionName).show();
        }
    }

    @Override public void onPermissionReallyDeclined(@NonNull String permissionName)
    {
        //result.setText("Permission " + permissionName + " can only be granted from SettingsScreen");
        Log.v("PERMISSION ReallyDeclined", "Permission " + permissionName + " can only be granted from settingsScreen");
        /** you can call  {@link PermissionHelper#openSettingsScreen(Context)} to open the settings screen */
    }

    @Override public void onNoPermissionNeeded()
    {
        //result.setText("Permission(s) not needed");
        Log.v("PERMISSION onNoPermissionNeeded", "Permission(s) not needed");
    }

    @Override public void onClick(View v)
    {

        permission_helper
                .request(Manifest.permission.SYSTEM_ALERT_WINDOW);/*you can pass it along other permissions,
                     just make sure you override OnActivityResult so you can get a callback.
                     ignoring that will result to not be notified if the user enable/disable the permission*/

    }

    public AlertDialog getAlertDialog(final String[] permissions, final String permissionName)
    {
        if (alert_dialog == null)
        {
            alert_dialog = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        alert_dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                permission_helper.requestAfterExplanation(permissions);
            }
        });
        alert_dialog.setMessage("Permissions need explanation (" + permissionName + ")");
        return alert_dialog;
    }

    public AlertDialog getAlertDialog(final String permission)
    {
        if (alert_dialog == null)
        {
            alert_dialog = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        alert_dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permission_helper.requestAfterExplanation(permission);
            }
        });
        alert_dialog.setMessage("Permission need explanation (" + permission + ")");
        return alert_dialog;
    }
    /*----------------end permission helper---------------------*/
}