package com.basar.administrator.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.basar.administrator.R;

public class MainActivity extends BasarActivity
{

    private TextView button_scanner;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponent();
        initVariable();
        initEvent();
        mainLogic();
    }

    public void onBackPressed()
    {
        return;
    }
    @Override
    protected  void initComponent()
    {
        button_scanner = (TextView) findViewById(R.id.button_scan_barcode);

    }
    @Override
    protected  void initVariable()
    {
    }

    @Override
    protected  void initEvent()
    {
        button_scanner.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MainActivity.this, ScanActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected  void mainLogic()
    {
    }

}
