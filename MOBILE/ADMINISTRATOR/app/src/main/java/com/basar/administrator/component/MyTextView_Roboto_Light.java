package com.basar.administrator.component;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView_Roboto_Light extends TextView
{

    public MyTextView_Roboto_Light(Context _context, AttributeSet _attrs, int _def_style)
    {
        super(_context, _attrs, _def_style);
        init();
    }

    public MyTextView_Roboto_Light(Context _context, AttributeSet _attrs)
    {
        super(_context, _attrs);
        init();
    }

    public MyTextView_Roboto_Light(Context _context)
    {
        super(_context);
        init();
    }

    private void init()
    {
        if (!isInEditMode())
        {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf");
            setTypeface(tf);
        }
    }

}