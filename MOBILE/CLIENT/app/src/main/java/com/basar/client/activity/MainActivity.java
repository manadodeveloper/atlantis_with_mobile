package com.basar.client.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.basar.client.R;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
