<?php
    /*
    |--------------------------------------------------------------------------
    | Init
    |--------------------------------------------------------------------------
    |Mengeset php ini variable
    |Inisialisasi variable sistem utama untuk pertama kali
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    // let's print the international format for the en_US locale
    setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
    date_default_timezone_set('Asia/Jakarta');

    setlocale(LC_ALL, 'IND');

    
    $result['result'] = 'error';//success or error
    $result['desc'] = "";
    
    $NO_PHOTO = "assets/img/no-photo.png";


	$CLIENT_ADDRESS = "";
    if(!isset($PAGE_ID))
    {
       $PAGE_ID = "";
    }

    //render html
    $CONTENTS = "";
    //bagian utama konten
    $CONTENT_MAIN = "";
    $LAYOUT_JS_CONTENT = "";
    $LAYOUT_CSS_CONTENT = "";
    //extended tambahan untuk komten
    $JS_EXTENDED = "";
    $CSS_EXTENDED = "";


    //header
    $TITLE_MAIN = "";
    $TITLE_SUB = "";
    $BUTTON_ACTION = "";

    
    $PAGE_MODEL = false;//jika akses ditolak adalah halaman model maka PAGE_MODEL = true;

    //group
    $GROUP['COMMON'] = "GRP00000000000000000";
    $GROUP['ADMIN'] = "SUPER_ADMINISTRATOR";




?>