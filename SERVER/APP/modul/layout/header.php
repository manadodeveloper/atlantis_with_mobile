<?php
    $LAYOUT_HEADER = "
                    
                    <section class='content-header text-white  fw-bold'>
                        <div class='row '>
                            <div class='col-lg-6 col-md-6 col-12'>

                                <h1>
                                    {$TITLE_MAIN}
                                    <small>{$TITLE_SUB}</small>
                                </h1>
                            </div>
                            <div class='col-lg-6 col-md-6 col-12'>

                                <div class='button-action'>
                                        {$BUTTON_ACTION}
                                </div>
                            </div>
                        </div>
                    </section>
    ";
?>