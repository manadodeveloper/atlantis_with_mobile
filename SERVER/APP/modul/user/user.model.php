<?PHP
     /**
      * transaksi.svr.php
      * 
      * File ini digunakan untuk administrasi transaksi
      *  
      *
      * @version         1.0
      * @author          basarudin
      * @created     feb 17 ,2013
      * @log
      *        - 24 maret 2014 create new file
      *
      * prefix parameter:
      *    n  - node
      *    o  - object
      *    a  - array
      *    s  - string
      *    b  - boolean
      *    f  - float
      *    i  - integer
      *    uk  - unknown
      *    fn - function
      *    _  - parameter
      **/
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.notification.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.notification.local.php");

    $oUser = new UserInfo();
    $oUser_i = new UserInfo();
    $oGroup = new Group();
    $oNotification = new Notification();

    $a_errors = array();
    $respone['status'] = 'error';
    $respone['desc'] = "";
    $PAGE_MODEL = true;


     
    if(isset($_REQUEST['action']))
    {

        if($_REQUEST['action'] == 'create')
        {

            $PAGE_ID = "USR104";
            require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
            if(!isset($_REQUEST['hak_akses']))
            {
                $a_errors[] = "hak akses harus dipilih";
            }
            else
            {
                if($_REQUEST['hak_akses'] == "GROUP_HELPDESK" || $_REQUEST['hak_akses'] == "GROUP_SPV_IT")
                {
                    $_REQUEST['wilayah'] = $_REQUEST['wilayah_helpdesk'];       
                }
            }
            if($_REQUEST['user-id'] == "")
            {
                $a_errors[] = "NIP tidak ada";
            }
            if($_REQUEST['real-name'] == "")
            {
                $a_errors[] = "nama harus diisi";
            }
            if($_REQUEST['username'] == "")
            {
                $a_errors[] = "username harus diisi";
            }
            if($_REQUEST['mail'] == "")
            {
                $a_errors[] = "email harus diisi";
            }
            if($_REQUEST['wilayah'] == "")
            {
                $a_errors[] = "wilayah harus diisi";
            }

            if($_REQUEST['password'] == "")
            {
                $a_errors[] = "password harus diisi";
            }
            if($_REQUEST['password-replay'] == "")
            {
                $a_errors[] = "password-ulang harus diisi";
            }

            if (!$a_errors) 
            {
                if($_REQUEST['password'] != $_REQUEST['password-replay'])
                {
                    $respone['status'] = 'error';
                    $respone['desc'] = "Password tidak sama";
                }
                else
                {

                    $s_where_check = " WHERE `username` = '{$_REQUEST['username']}' or userID = '{$_REQUEST['user-id']}' ";
                    if($oUser->getUserCount($s_where_check)> 0)
                    {
                        $respone['status'] = 'error';
                        $respone['desc'] = "NIP / username  sudah pernah dimasukan";
                    }
                    else
                    {
                        $a_hak_akses = array();
                        if($_REQUEST['hak_akses'] != "")
                        {
                            $a_hak_akses[]  = $_REQUEST['hak_akses'];
                        }
                        foreach ($_REQUEST as $key => $value) {
                            $_REQUEST[$key] == $oUser_i->antiInjection($value);
                        }
                        if($oUser_i->create(
                            $a_hak_akses,
                            $_REQUEST['user-id'],
                            $_REQUEST['username'],
                            $_REQUEST['password'],
                            $_REQUEST['real-name'],
                            $_REQUEST['mail'],
                            $_REQUEST['wilayah']
                        ))
                        {
                            $respone['status'] = 'success';
                            $respone['desc'] = "{$_REQUEST['real-name']} berhasil dimasukan";
                            $respone['user_id'] = $_REQUEST['user-id'];

                            $s_notif_message = "{$USER[0]['realName']} membuat pengguna baru dengan nama <span class='label label-success'>{$_REQUEST['real-name']}</span>";
                            
                            create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);
                        }
                        else
                        {
                            $respone['status'] = 'error';
                            $respone['desc'] = "user gagal dimasukan";
                        }
                    }
                }

            }
            else
            {
                $s_error =  '';
                foreach ($a_errors as $error) 
                {
                    $s_error .= "$error<br />";
                }
                $respone['status'] = 'error';
                $respone['desc'] = $s_error;
            }
            echo json_encode($respone);

        }

        elseif($_REQUEST['action'] == 'aktifasi')
        {

            $PAGE_ID = "USR106";
            require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
            if($_REQUEST['user-id'] == "")
            {
                $a_errors[] = "NIP tidak ada";
            }
            if (!$a_errors) 
            {        
                if($oUser->setActive($_REQUEST['user-id'] ))
                {
                    $respone['status'] = 'success';
                    $respone['desc'] = " user berhasil di aktifkan";

                    $s_notif_message = "{$USER[0]['realName']} berhasil mengaktifasi pengguna  <span class='label label-success'>{$_REQUEST['user-id']}</span>";
                    create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);

                }
                else
                {
                    $respone['status'] = 'error';
                    $respone['desc'] = "  gagal diaktifasi";
                }
                    
            }
            else
            {
                $s_error =  '';
                foreach ($a_errors as $error) {
                    $s_error .= "$error<br />";
                }
                $respone['status'] = 'error';
                $respone['desc'] = $s_error;
            }
            echo json_encode($respone);

        }
        elseif($_REQUEST['action'] == 'deaktifasi')
        {
            
            $PAGE_ID = "USR106";
            require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
            if(!isset($_REQUEST['user-id']))
            {
                $a_errors[] = "NIP tidak ada";
            }
            else
            {
                if($_REQUEST['user-id'] == "")
                {
                    $a_errors[] = "NIP tidak ada";
                }
            }
            if (!$a_errors) 
            {        
                if($oUser->setNonActive($_REQUEST['user-id'] ))
                {
                    $respone['status'] = 'success';
                    $respone['desc'] = " user berhasil di non-aktifkan";
                    $s_notif_message = "{$USER[0]['realName']} berhasil menonaktifkan pengguna  <span class='label label-danger'>{$_REQUEST['user-id']}</span>";
                    create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);

                }
                else
                {
                    $respone['status'] = 'error';
                    $respone['desc'] = "  gagal deaktifasi";
                }
                    
            }
            else
            {
                $s_error =  '';
                foreach ($a_errors as $error) {
                    $s_error .= "$error<br />";
                }
                $respone['status'] = 'error';
                $respone['desc'] = $s_error;
            }
            echo json_encode($respone);

        }
        elseif($_REQUEST['action'] == 'reset')
        {
            
            $PAGE_ID = "USR106";
            require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
            if($_REQUEST['user-id'] == "")
            {
                $a_errors[] = "NIP tidak ada";
            }
            if (!$a_errors) 
            {

                if($oUser->setNonActive($_REQUEST['user-id'] ))
                {
                    $respone['status'] = 'success';
                    $respone['desc'] = " User berhasil di nonaktifkan";

                    $s_notif_message = "{$USER[0]['realName']} berhasil mereset pengguna  <span class='label label-danger'>{$_REQUEST['user-id']}</span>";
                    create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);

                    //dapatkan data user
                    $a_user_detaiil =  $oUser->getUserDetail($_REQUEST['user-id'] );
                    //kirim notifikasi
                    $s_message = str_replace("[USER]", $a_user_detaiil[0]['realName'], $MESSAGE_ADMIN_RESET);
                    sendNotification($ADMIN_KOPERASI,$s_message,$CANGKRUK);
                    $user_koperasi[]['messenger'] = $a_user_detaiil[0]['messenger'];
                    sendNotification($user_koperasi,$MESSAGE_USER_RESET,$CANGKRUK);
                 
                }
                else
                {
                     $respone['status'] = 'error';
                     $respone['desc'] = " ' gagal direset";
                }

            }
            else
            {
                $s_error =  '';
                foreach ($a_errors as $error) 
                {
                     $s_error .= "$error<br />";
                }
                $respone['status'] = 'error';
                $respone['desc'] = $s_error;
            }
            echo json_encode($respone);
        }

        if($_REQUEST['action'] == 'update_privileges')
        {
            
            $PAGE_ID = "USR105";
            require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
            if($oGroup->update($_REQUEST['user-id'],$_REQUEST['group']))
            {
                $respone['status'] = 'success';
                $respone['desc'] = "Hak akses berhasil di update";
                $s_notif_message = "{$USER[0]['realName']} berhasil mengubah hak akses  <span class='label label-warning'>{$_REQUEST['user-id']}</span>";
                create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);
            }
            else
            {
                $respone['status'] = 'error';
                $respone['desc'] = "Hak akses gagal  diupdate";

            }
            echo json_encode($respone);
        }
        

        if($_REQUEST['action'] == 'ganti_password')
        {
            
            $PAGE_ID = "USR112";
            require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
            if(!isset($_REQUEST['password_lama']))
            {
                $a_errors[] = "password lama tidak ada";
            }
            elseif($_REQUEST['password_lama'] == "")
            {
                $a_errors[] = "password lama harus diisi";
            }


            if(!isset($_REQUEST['password_baru']))
            {
                $a_errors[] = "password baru tidak ada";
            }
            elseif($_REQUEST['password_baru'] == "")
            {
                $a_errors[] = "password lama harus diisi";
            }


            if(!isset($_REQUEST['password_ulang']))
            {
                $a_errors[] = "password ulang  tidak ada";
            }
            elseif($_REQUEST['password_ulang'] == "")
            {
                $a_errors[] = "password ulang harus diisi";
            }

            if (!$a_errors) 
            {
                //cocokan input password baru dengan password ulang
                if($_REQUEST['password_ulang'] != $_REQUEST['password_baru'])
                {
                    $respone['status'] = 'error';
                    $respone['desc'] = " pasword baru dan password ulang tidak sama";
                }
                else
                {
                    if($USER[0]['password'] != md5($_REQUEST['password_lama']))
                    {

                        $respone['status'] = 'error';
                        $respone['desc'] = " pasword lama tidak sesuai dengan yang ada di sistem";
                    }
                    else
                    {
                        if($oUser->setUserPassword($USER[0]['userID'],$_REQUEST['password_baru']))
                        {
                            $respone['status'] = 'success';
                            $respone['desc'] = " password sudah diganti";


                            //notif ke admin
                            $notif_user_to =  $USER[0]['userID']; 
                            $notif_title  = "GANTI PASSWORD";
                            $notif_message = "anda mengganti password  ";
                            $notif_link  = "";
                            $notif_user_from = "";
                            $oNotification->create(
                                $notif_title ,
                                $notif_message ,
                                $notif_link ,
                                $notif_user_to ,
                                "" ,
                                $notif_user_from ,
                                "" 
                            );    

                        }
                        else
                        {
                            $respone['status'] = 'error';
                            $respone['desc'] = " password tidak dapat diganti. harap hubungi admin";
                        }
                    }
                }

                

            }
            else
            {
                $s_error =  '';
                foreach ($a_errors as $error) 
                {
                     $s_error .= "$error<br />";
                }
                $respone['status'] = 'error';
                $respone['desc'] = $s_error;
            }
            echo json_encode($respone);
        }
        elseif($_REQUEST['action'] == "profilepicture")
        {
            
            $PAGE_ID = "USR113";
            require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
            $folder_upload = $SYSTEM['DIR_MODUL_UPLOAD']."/image/";
            $uploadedFile = '';
            if(!empty($_FILES["file"]["type"])){
                $file_name = time().'_'.$_FILES['file']['name'];
                $valid_extensions = array("jpeg", "jpg", "png");
                $temporary = explode(".", $_FILES["file"]["name"]);
                $file_extension = end($temporary);
                if((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions))
                {
                    $source_path = $_FILES['file']['tmp_name'];

                    $target_path = $folder_upload.$_REQUEST['user-id']."_".md5($file_name).".".$file_extension;
                    if(move_uploaded_file($source_path,$target_path)){
                        $uploadedFile = $file_name;
                        $db_photo_path = str_replace($SYSTEM['DIR_PATH']."/", "", $target_path);
                        if($oUser->setPhoto($_REQUEST['user-id'],$db_photo_path))
                        {
                            $respone['status'] ="success";
                            $respone['desc'] ="file sudah diupload";
                            $respone['filename'] = $file_name;
                            $respone['db_path'] = $db_photo_path;  
                        }
                        else
                        {

                            $respone['status'] ="error";
                            $respone['desc'] ="file tidak dapat disimpan di db";
                        }
                    }
                    else
                    {
                        $respone['status'] ="error";
                        $respone['desc'] ="file tidak dapat diupload";
                    }
                }
                else
                {
                    $respone['status'] ="error";
                    $respone['desc'] ="file harus berupa gambar";
                }
            }
            else
            {
                $respone['status'] ="error";
                $respone['desc'] ="file tidak bertipe";
            }
            echo json_encode($respone);
        }


          
    }
    $oUser->closeDB();
    $oUser_i->closeDB();
    $oGroup->closeDB();
    $oNotification->closeDB();

?>