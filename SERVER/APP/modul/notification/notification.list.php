<?php
    /*
    |--------------------------------------------------------------------------
    | notification view
    |--------------------------------------------------------------------------
    |view  modul notification register
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    require_once($SYSTEM['DIR_PATH']."/class/class.notification.php");
    $oNotification = new Notification();


    $a_title[] = "";
    $a_title_class[] = " ";
    $s_table_container = "";
    $s_condition = " WHERE notifUserTo = '{$USER[0]['userID']}' AND `notifStatus` != 2 ";
    $s_limit = "  ";
    $s_order = " ORDER BY notifDateCreate DESC";


    $LAYOUT_JS_EXTENDED .= "
                    


                    ";
    $LAYOUT_CSS_EXTENDED .= "

                    ";
                    $a_data = $oNotification->getList($s_condition, $s_order, $s_limit);
    if(isset($a_data))
    {
          
        $s_table_container ="";
        $s_table_container .="
                    <table  id='table-notification' class='table  table-bordered table-hover' width='100%'  border='1px'>
                        ";
        $s_table_container .="<tbody>";

        for($i=0;$i<count($a_data);$i++)
        {

                    
            $a_data[$i]['notifMessage'] = str_replace('&lt;', "<", $a_data[$i]['notifMessage']);
            $a_data[$i]['notifMessage'] = str_replace('&gt;', ">", $a_data[$i]['notifMessage']);
            $a_data[$i]['notifMessage'] = str_replace('`', "'", $a_data[$i]['notifMessage']);
            //untuk informasi jumlah soal
            $s_table_container .="<tr >";
            $s_table_container .= "<td  align='left'>"
                                     .strtoupper ($a_data[$i]['notifDateCreate'])."<br>"
                                     ."<p class='hostname'>".$a_data[$i]['notifMessage']."</p>"
                                ."</td>";
            $s_table_container .="</tr>";
        }    
        $s_table_container .="</tbody>";
        $s_table_container .="</table>";
    }
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>NOTIFIKASI</h4>
                                </div>
                                <div style='float:right'></div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_table_container}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oNotification->closeDB();

?>