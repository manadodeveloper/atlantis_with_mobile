$(document).ready(function() {

    /*************************************************
    |   BEGIN TABLE-COMPUTER
    **************************************************/
    if($('#table-device').length)
    {
        
        $('#table-device').DataTable
        ({        
            rowReorder: true,
            columnDefs: [
                { orderable: true, className: 'reorder', targets:[ 0, 2 ] },
                { orderable: false, targets: '_all' }
            ],
            lengthMenu: [[10, 25, -1], [10, 25, "All"]],
             order: [[ 2, "desc" ]]
        });


        $('#table-device tbody').on( 'click', '.button-device-activation', function () 
        {
            device_register_id = $(this).attr('record-id');
            //alert(device_register_id);
            $.ajax({
                type: 'POST',
                url: "index.php?page=device&type=model&action=aktifasi",
                dataType:"json",
                data: 'durID='+device_register_id,
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                        return false;
                    },
                success:
                    function(msg,status)
                    {
                        //alert(msg);

                        if(msg.status == 'success')
                        {

                            Swal.fire({
                                type: 'success',
                                title: '',
                                text: msg.desc,
                                timer: 1200,
                                onOpen: () => {
                                    var zippi = new Audio('assets/sound/success.mp3')
                                    zippi.play();
                                },
                                onClose: () => {
                                    document.location='index.php?page=device&action=list';
                                }
                            });

                        }
                        else
                        {
                            Swal.fire
                            ({
                                type: 'error',
                                title: '',
                                html: msg.desc,
                                onOpen: () => {
                                    var zippi = new Audio('assets/sound/error.mp3')
                                    zippi.play();
                                }
                            });
                        }
                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {

                        Swal.fire
                        ({
                            type: 'error',
                            title: '',
                            text: 'Terjadi error saat proses kirim data',
                            onOpen: () => 
                            {
                                var zippi = new Audio('assets/sound/error.mp3')
                                zippi.play();
                            }
                        });
                    }
            });//end of ajax
        });

        $('#table-device tbody').on( 'click', '.button-device-delete', function () 
        {

            device_register_id = $(this).attr('record-id');
            perangkat = $(this).parent().parent().find(".perangkat").html(); 
            //alert(device_register_id);
            //alert(perangkat);
            //$('#modal-content').html('apakah yakin menghapus perangkat <b>'+perangkat+'</b> ?');
            //$('#modal-container').modal('show');

            Swal.fire({
                    title: 'Konfirmasi',
                    html: 'apakah yakin menghapus perangkat <b>'+perangkat+'</b> ?',
                    type: 'warning',
                    onOpen: () => {
                        var zippi = new Audio('assets/sound/warning.mp3')
                        zippi.play();
                    },
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, dihapus saja',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) 
                    {
                        $.ajax({
                            type: 'POST',
                            url: "index.php?page=device&type=model&action=delete",
                            dataType:"json",
                            data: 'durID='+device_register_id,
                            cache:false,
                            //jika complete maka
                            complete:
                                function(data,status)
                                {
                                 return false;
                                },
                            success:
                                function(msg,status)
                                {
                                    //alert(msg);
                                    if(msg.status == 'success')
                                    {

                                        Swal.fire({
                                            type: 'success',
                                            title: '',
                                            text: msg.desc,
                                            timer: 1200,
                                            onOpen: () => {
                                                var zippi = new Audio('assets/sound/success.mp3')
                                                zippi.play();
                                            },
                                            onClose: () => {
                                                document.location='index.php?page=device';
                                            }
                                        });

                                    }
                                    else
                                    {
                                        Swal.fire
                                        ({
                                            type: 'error',
                                            title: '',
                                            html: msg.desc,
                                            onOpen: () => {
                                                var zippi = new Audio('assets/sound/error.mp3')
                                                zippi.play();
                                            }
                                        });
                                    }
                                },
                            //untuk sementara tidak digunakan
                            error:
                                function(msg,textStatus, errorThrown)
                                {
                                    Swal.fire
                                    ({
                                        type: 'error',
                                        title: '',
                                        text: 'Terjadi error saat proses kirim data',
                                        onOpen: () => 
                                        {
                                            var zippi = new Audio('assets/sound/error.mp3')
                                            zippi.play();
                                        }
                                    });
                                }
                        });//end of ajax
                    }
            });
        });


        $('#table-device tbody').on( 'click', '.button-send-message', function () 
        {
            $('#notification-container').hide(); 
            device_register_id = $(this).attr('record-id');
            $.ajax({
                type: 'POST',
                url: "index.php?page=device&type=model&action=testnotif",
                dataType:"json",
                data: 'device-id='+device_register_id,
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                        return false;
                    },
                success:
                    function(msg,status)
                    {
                        //alert(msg);
                        if(msg.status == 'success')
                        {
                            Swal.fire({
                                type: 'success',
                                title: '',
                                text: msg.desc,
                                timer: 1200,
                                onOpen: () => {
                                    var zippi = new Audio('assets/sound/success.mp3')
                                    zippi.play();
                                },
                                onClose: () => {
                                    //document.location='index.php?page=welcome';
                                }
                            });
                        }
                        else
                        {
                            Swal.fire
                            ({
                                type: 'error',
                                title: '',
                                html: msg.desc,
                                onOpen: () => {
                                    var zippi = new Audio('assets/sound/error.mp3')
                                    zippi.play();
                                }
                            });
                        }
                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                        //alert('asa');
                        Swal.fire
                        ({
                            type: 'error',
                            title: '',
                            text: 'Terjadi error saat proses kirim data',
                            onOpen: () => 
                            {
                                var zippi = new Audio('assets/sound/error.mp3')
                                zippi.play();
                            }
                        });
                    }
            });//end of ajax
        });

    }
    /*   END TABLE-COMPUTER
    **************************************************/

});